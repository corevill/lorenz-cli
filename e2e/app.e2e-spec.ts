import { LorenzCliPage } from './app.po';

describe('lorenz-cli App', () => {
  let page: LorenzCliPage;

  beforeEach(() => {
    page = new LorenzCliPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
