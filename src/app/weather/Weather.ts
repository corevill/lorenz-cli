export class Weather {
  id: number;
  name: string;
  day: Date;
  sunrise: string;
  sunset: string;
  period: number;
  sky_state: string;
  rain: number;
  rain_probability: number;
  storm_probability: number;
  snow: number;
  snow_probability: number;
  thermal_sensation: number;
  relative_humidity: number;
  wind_velocity: number;
  wind_direction: string;
  temperature: number;
}
