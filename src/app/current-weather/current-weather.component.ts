import { Component } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { WeatherService } from '../weather-service/weather-service.component';
import { Weather } from '../weather/Weather';
import { WeatherUtils } from '../weather-utils/weather-utils';

@Component({
  selector: 'current-weather',
  templateUrl: './current-weather.component.html',
  styleUrls: ['./current-weather.component.css']
})
export class CurrentWeather {
  constructor(private weatherService: WeatherService) {}

  currentWeather: Weather = new Weather();
  currentHour: number;
  hour: string;
  date: string;
  imageSrc: string;

  ngOnInit(){
        this.updateCurrentWeather();
        WeatherUtils.addCurrentWeatherChangeListeners(() => {this.updateCurrentWeather()});

        WeatherUtils.addTimerListeners(() => {
          this.date = WeatherUtils.DATE;
          this.hour = WeatherUtils.HOUR;
        });
  }

  updateCurrentWeather(){
    this.weatherService.getCurrentWeather().subscribe(currentWeather => {
      this.currentWeather = currentWeather[0];
      this.imageSrc = WeatherUtils.SKY_STATES_IMAGES[this.currentWeather.sky_state];
    });
  }
}
