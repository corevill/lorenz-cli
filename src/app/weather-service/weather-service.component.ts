import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Weather } from '../weather/Weather';

@Injectable()
export class WeatherService {
  constructor (
    private http: Http
  ) {}

  getWeathers() {
    // return this.http.get(`./assets/hourly_forecasts.json`).map((res:Response) => this.restonseToJSON(res));
    return this.http.get(`https://lorenz-aemet.herokuapp.com/hourly_forecasts`).map((res:Response) => this.restonseToJSON(res));
  }

  getCurrentWeather() {
    // return this.http.get(`./assets/hourly_forecasts-current.json`).map((res:Response) => this.restonseToJSON(res));
    return this.http.get(`http://lorenz-aemet.herokuapp.com/hourly_forecast/current`).map((res:Response) => this.restonseToJSON(res));
  }

  getSkyStatesImages(){
    return this.http.get(`./assets/sky-states.json`).map((res:Response) => this.restonseToJSON(res));
  }

  restonseToJSON(rest){
    return JSON.parse(rest._body);
  }
}
