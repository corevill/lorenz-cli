import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {CurrentWeather} from './current-weather/current-weather.component'
import { NextWeather } from './next-weather/next-weather.component'
import { NextWeatherItem } from './next-weather/next-weather-item/next-weather-item.component'
import { WeatherService } from './weather-service/weather-service.component'
import { WeatherUtils } from './weather-utils/weather-utils';

import {MdToolbarModule} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    CurrentWeather,
    NextWeather,
    NextWeatherItem
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MdToolbarModule
  ],
  providers: [WeatherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
