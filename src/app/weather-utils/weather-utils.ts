import { Observable } from 'rxjs/Rx';

export class WeatherUtils {
  public static WEEKDAYS = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
  public static SKY_STATES_IMAGES;

  public static HOUR: string;
  public static DATE: string;
  private static currentHour: number;

  public static currentWeatherChangeListeners = new Array();
  public static dayChangeListeners = new Array();
  public static timerListeners = new Array();

  public static setSkyStatesImages(skyStatesImages){
    WeatherUtils.SKY_STATES_IMAGES = skyStatesImages;
  }

  public static addCurrentWeatherChangeListeners(listener){
    WeatherUtils.currentWeatherChangeListeners.push(listener);
  }

  public static addTimerListeners(listener){
    WeatherUtils.timerListeners.push(listener);
  }

  public static addDayChangeListeners(listener){
    WeatherUtils.dayChangeListeners.push(listener);
  }

  public static clockStart(){
      WeatherUtils.addTimerListeners(WeatherUtils.update);

      let timer = Observable.timer(1,1000);
      timer.subscribe(() => WeatherUtils.eventTimerListeners());
  }

  private static update() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();

    if(WeatherUtils.DATE === '' || WeatherUtils.currentHour !== h){
      WeatherUtils.eventCurrentWeatherChange();
      if(h === 0){
        WeatherUtils.eventDayChange();
      }
    }

    WeatherUtils.currentHour = h;
    WeatherUtils.DATE = WeatherUtils.getCurrentDate();
    WeatherUtils.HOUR = WeatherUtils.WEEKDAYS[today.getDay()] + ", " + h + ":" + WeatherUtils.checkTime(m) + ":" + WeatherUtils.checkTime(s);
  }

  private static checkTime(i: number) {
    var result = i.toString();

    if (result.length == 1) {
      result = '0' + i;
    }
    return result;
  }

  private static getCurrentDate(){
    var d = new Date();
    return [WeatherUtils.addZeroToLeft(d.getDate()), WeatherUtils.addZeroToLeft(d.getMonth()+1), d.getFullYear()].join('/');
  }

  private static addZeroToLeft(s) {
    return (s < 10) ? '0' + s : s;
  }

  private static eventTimerListeners(){
    WeatherUtils.timerListeners.map(listener => listener.apply());
  }

  private static eventCurrentWeatherChange(){
    WeatherUtils.currentWeatherChangeListeners.map(listener => listener.apply());
  }

  private static eventDayChange(){ 
    WeatherUtils.dayChangeListeners.map(listener => listener.apply());
  }
}
