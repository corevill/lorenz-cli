import { Component } from '@angular/core';
import { CurrentWeather } from './current-weather/current-weather.component'
import { NextWeather } from './next-weather/next-weather.component'
import { WeatherUtils } from './weather-utils/weather-utils';
import { WeatherService } from './weather-service/weather-service.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private weatherService: WeatherService){};
  title = 'Lorenz cli using Material design!';

  ngOnInit() {
    WeatherUtils.clockStart();
    this.weatherService.getSkyStatesImages().subscribe(skyStatesImages => WeatherUtils.setSkyStatesImages(skyStatesImages));
  }
}
 
