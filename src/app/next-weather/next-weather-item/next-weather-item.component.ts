import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { WeatherService } from '../../weather-service/weather-service.component';
import { Weather } from '../../weather/Weather';
import { WeatherUtils } from '../../weather-utils/weather-utils';

@Component({
  selector: 'next-weather-item',
  templateUrl: './next-weather-item.component.html',
  styleUrls: ['./next-weather-item.component.css']
})
export class NextWeatherItem {
  constructor(private weatherService: WeatherService) {}

  @Input() weather: Weather;
  imageSrc: string;
  period: number;

  ngOnInit(){
      this.period = this.weather.period;
      this.imageSrc = WeatherUtils.SKY_STATES_IMAGES[this.weather.sky_state];
  } 
}
