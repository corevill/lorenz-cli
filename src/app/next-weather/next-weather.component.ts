import { Component } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { NextWeatherItem } from './next-weather-item/next-weather-item.component'
import { WeatherService } from '../weather-service/weather-service.component';
import { WeatherUtils } from '../weather-utils/weather-utils';

@Component({
  selector: 'next-weather',
  templateUrl: './next-weather.component.html',
  styleUrls: ['./next-weather.component.css']
})
export class NextWeather {
  constructor(private weatherService: WeatherService) {}

  maxNextWeathers = 7;

  weathers = [];
  weathersToPrint = [];
  period: number;

  ngOnInit(){
    this.updateWeathers();
    WeatherUtils.addDayChangeListeners(() => this.updateWeathers());
    WeatherUtils.addCurrentWeatherChangeListeners(() => this.updateCurrentPeriod());
  }

  updateCurrentPeriod(){
    this.weatherService.getCurrentWeather().subscribe(currentWeather => {
      var cont = 0;
      this.weathersToPrint = [];
      this.period = currentWeather[0].period;

      this.weathers.map(w => {
        if(this.isStringDateEqualsToToday(w.day) && this.checkPeriod(w.period) && cont < this.maxNextWeathers){
          this.weathersToPrint.push(w);
          cont++;
        }
      });
    });
  }

  checkPeriod(period){
    return period > this.period || period > 16 && period <= 23 && this.period > 16 && this.period <= 23;
  }

  isStringDateEqualsToToday(date){
    return new Date(date).getDate() === new Date().getDate();
  }

  updateWeathers(){
    this.weatherService.getWeathers().subscribe(response => {
      this.weathers = [];

      response.map(w => this.weathers.push({
          id: w.id,
          name: w.name,
          day: w.day,
          sunrise: w.sunrise,
          sunset: w.sunset,
          period: w.period,
          sky_state: w.sky_state,
          rain: w.rain,
          rain_probability: w.rain_probability,
          storm_probability: w.storm_probability,
          snow: w.snow,
          snow_probability: w.snow_probability,
          thermal_sensation: w.thermal_sensation,
          relative_humidity: w.relative_humidity,
          wind_velocity: w.wind_velocity,
          wind_direction: w.wind_direction,
          temperature: w.temperature
      }));

      this.updateCurrentPeriod();
    });

  }
}
